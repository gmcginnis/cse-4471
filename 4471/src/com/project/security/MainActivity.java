package com.project.security;

import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class MainActivity extends Activity {
	
	public static String inputLine;
	public int interval;
	public TimeUnit timeUnit;
	private final static String ADDRESS = "http://www.cse.ohio-state.edu/~champion/4471/index.html";/*"192.168.1.177"*/
	public final String TAG = "MainActivity";
	private final String htmlTag = "b";
	private static boolean active,safeAlert;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	ScheduledFuture<?> dataHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		safeAlert=false; interval=10; timeUnit=TimeUnit.SECONDS;

		final Runnable getData = new Runnable() {
            public void run() { new getHTML().execute(); }
        };
        dataHandler = scheduler.scheduleAtFixedRate(getData, 0, interval, timeUnit);//creates a schedule to fetch data at given interval (default)
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		active = true;
		new getHTML().execute();		
	}
	
	@Override
	protected void onPause() {
		active = false;//no longer visible
		safeAlert=false;
		super.onPause();	
	}
	
	@Override
	protected void onStop() {
		active = false;//no longer visible
		safeAlert=false;
		super.onStop();	
	}
	
	@Override
	protected void onDestroy() {
		dataHandler.cancel(true);//stops fetching data when the app is killed
		scheduler.shutdown();
		super.onDestroy();	
	}
	
	//gets the most recent data value.
	public void getLast() {
		String t = new String();
		StringTokenizer st = new StringTokenizer(inputLine);
		while(st.hasMoreTokens()) {
			t = st.nextToken();
		}
		inputLine = t;
	}
	//method that prompts user the very first time the app runs for a number to call in case of dangerous levels
	
	//Updates the data value displayed on the app
	private void PrintData () {
		Log.i("MainActibity", "data fetch");
		getLast();
		TextView t = new TextView(this);
		t=(TextView)findViewById(R.id.textField1);
		t.setGravity(android.view.Gravity.CENTER);
		Random generator = new Random();inputLine= Integer.toString(generator.nextInt(800) + 1);//creates random numbers for testing
		
		//this changes the color of the on-screen text with how dangerous the water level is ***needs our own data
		if(Integer.parseInt(inputLine) <= 300) {//safe
			t.setTextColor(android.graphics.Color.GREEN);
			safeAlert=false;
		}
		else if((Integer.parseInt(inputLine) > 300) && (Integer.parseInt(inputLine) <= 500)) {//caution
			t.setTextColor(android.graphics.Color.YELLOW);
			safeAlert=false;
		}
		else {//WARNING
			t.setTextColor(android.graphics.Color.RED);
			PendingIntent pi = PendingIntent.getActivity(this, 0,  new Intent(this, MainActivity.class), 0);
			if (active && (!safeAlert)){
				//creates dialog box with warning if activity is active and an alert hasn't already shown
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.setMessage("WARNING!\nYour water has reached a dangerous level of impurity.\nNotify the proper authorities immediately.");
				alert.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, closes dialog box
						dialog.dismiss();
					}
				  });
				alert.create();
				alert.show();
				safeAlert=true;//flag to ensure warning dialog only shows once while active
			}
			else if (!active){
				//sends out dangerous level notification if app is not open
				Notification noti = new Notification.Builder(this)
				.setContentTitle("Dangerous Level!")
				.setContentText("Water purity warning.")
				.setAutoCancel(true)
				.addAction (R.drawable.ic_launcher, "More...", pi)
				.build();
				NotificationManager notificationManager = 
						(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				notificationManager.notify(0, noti);
			}
		}
		t.setText(inputLine);
	}//still needs text describing what the number means e.g. "this is your contaminate level: ____ ppm"

	//Background task which retrieves data from web
	class getHTML extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				Document doc = Jsoup.connect(ADDRESS).get();
				inputLine = doc.getElementsByTag(htmlTag).text();
			} catch (Exception e) { //if the network connection fails, show dialog
				Log.e(TAG, "Failed to load HTML code", e);
				
				//creates an alert if network connection fails
				AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
				alert.setMessage("Error connecting to the network.");
				alert.setPositiveButton("Retry?",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, retry network connection
						MainActivity.this.onCreate(new Bundle());
					}
				  });
				alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, app closes
						MainActivity.this.finish();
					}
				  });
				alert.create();
				alert.show();
			}
			return null;
		}
		@Override
        protected void onPostExecute(Void result) {
              MainActivity.this.PrintData();
        }
	}
	
}

